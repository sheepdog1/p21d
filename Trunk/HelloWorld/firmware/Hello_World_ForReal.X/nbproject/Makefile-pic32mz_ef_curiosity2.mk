#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-pic32mz_ef_curiosity2.mk)" "nbproject/Makefile-local-pic32mz_ef_curiosity2.mk"
include nbproject/Makefile-local-pic32mz_ef_curiosity2.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=pic32mz_ef_curiosity2
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/config/pic32mz_ef_curiosity2/peripheral/clk/plib_clk.c ../src/config/pic32mz_ef_curiosity2/peripheral/evic/plib_evic.c ../src/config/pic32mz_ef_curiosity2/peripheral/gpio/plib_gpio.c ../src/config/pic32mz_ef_curiosity2/peripheral/uart/plib_uart6.c ../src/config/pic32mz_ef_curiosity2/stdio/xc32_monitor.c ../src/config/pic32mz_ef_curiosity2/initialization.c ../src/config/pic32mz_ef_curiosity2/interrupts.c ../src/config/pic32mz_ef_curiosity2/exceptions.c ../src/main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1746154667/plib_clk.o ${OBJECTDIR}/_ext/1703710942/plib_evic.o ${OBJECTDIR}/_ext/1703657114/plib_gpio.o ${OBJECTDIR}/_ext/1703254171/plib_uart6.o ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o ${OBJECTDIR}/_ext/1205694195/initialization.o ${OBJECTDIR}/_ext/1205694195/interrupts.o ${OBJECTDIR}/_ext/1205694195/exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1746154667/plib_clk.o.d ${OBJECTDIR}/_ext/1703710942/plib_evic.o.d ${OBJECTDIR}/_ext/1703657114/plib_gpio.o.d ${OBJECTDIR}/_ext/1703254171/plib_uart6.o.d ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o.d ${OBJECTDIR}/_ext/1205694195/initialization.o.d ${OBJECTDIR}/_ext/1205694195/interrupts.o.d ${OBJECTDIR}/_ext/1205694195/exceptions.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1746154667/plib_clk.o ${OBJECTDIR}/_ext/1703710942/plib_evic.o ${OBJECTDIR}/_ext/1703657114/plib_gpio.o ${OBJECTDIR}/_ext/1703254171/plib_uart6.o ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o ${OBJECTDIR}/_ext/1205694195/initialization.o ${OBJECTDIR}/_ext/1205694195/interrupts.o ${OBJECTDIR}/_ext/1205694195/exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o

# Source Files
SOURCEFILES=../src/config/pic32mz_ef_curiosity2/peripheral/clk/plib_clk.c ../src/config/pic32mz_ef_curiosity2/peripheral/evic/plib_evic.c ../src/config/pic32mz_ef_curiosity2/peripheral/gpio/plib_gpio.c ../src/config/pic32mz_ef_curiosity2/peripheral/uart/plib_uart6.c ../src/config/pic32mz_ef_curiosity2/stdio/xc32_monitor.c ../src/config/pic32mz_ef_curiosity2/initialization.c ../src/config/pic32mz_ef_curiosity2/interrupts.c ../src/config/pic32mz_ef_curiosity2/exceptions.c ../src/main.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-pic32mz_ef_curiosity2.mk dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MZ2048EFM144
MP_LINKER_FILE_OPTION=,--script="..\src\config\pic32mz_ef_curiosity2\p32MZ2048EFM144.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1746154667/plib_clk.o: ../src/config/pic32mz_ef_curiosity2/peripheral/clk/plib_clk.c  .generated_files/708a25d1bd61c135da4358a613a93c2a002944e7.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1746154667" 
	@${RM} ${OBJECTDIR}/_ext/1746154667/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1746154667/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1746154667/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1746154667/plib_clk.o ../src/config/pic32mz_ef_curiosity2/peripheral/clk/plib_clk.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1703710942/plib_evic.o: ../src/config/pic32mz_ef_curiosity2/peripheral/evic/plib_evic.c  .generated_files/ebdfd663036e186e1ed9e6f4816cc567a1c9a041.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1703710942" 
	@${RM} ${OBJECTDIR}/_ext/1703710942/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1703710942/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1703710942/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1703710942/plib_evic.o ../src/config/pic32mz_ef_curiosity2/peripheral/evic/plib_evic.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1703657114/plib_gpio.o: ../src/config/pic32mz_ef_curiosity2/peripheral/gpio/plib_gpio.c  .generated_files/16d732b64d7e4a28a99cdf65ea2a94304d4870d6.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1703657114" 
	@${RM} ${OBJECTDIR}/_ext/1703657114/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1703657114/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1703657114/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1703657114/plib_gpio.o ../src/config/pic32mz_ef_curiosity2/peripheral/gpio/plib_gpio.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1703254171/plib_uart6.o: ../src/config/pic32mz_ef_curiosity2/peripheral/uart/plib_uart6.c  .generated_files/523015cbf1af7b7fd1514485e4bf6595c17cfe6a.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1703254171" 
	@${RM} ${OBJECTDIR}/_ext/1703254171/plib_uart6.o.d 
	@${RM} ${OBJECTDIR}/_ext/1703254171/plib_uart6.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1703254171/plib_uart6.o.d" -o ${OBJECTDIR}/_ext/1703254171/plib_uart6.o ../src/config/pic32mz_ef_curiosity2/peripheral/uart/plib_uart6.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1461993363/xc32_monitor.o: ../src/config/pic32mz_ef_curiosity2/stdio/xc32_monitor.c  .generated_files/fcc031efb3b3feef6885a954ab578d89aea36dc7.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1461993363" 
	@${RM} ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1461993363/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o ../src/config/pic32mz_ef_curiosity2/stdio/xc32_monitor.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1205694195/initialization.o: ../src/config/pic32mz_ef_curiosity2/initialization.c  .generated_files/c9c1e8333a7deaf359089f8a89baa5688f69707b.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1205694195" 
	@${RM} ${OBJECTDIR}/_ext/1205694195/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1205694195/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1205694195/initialization.o.d" -o ${OBJECTDIR}/_ext/1205694195/initialization.o ../src/config/pic32mz_ef_curiosity2/initialization.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1205694195/interrupts.o: ../src/config/pic32mz_ef_curiosity2/interrupts.c  .generated_files/1fe54ce6a6d2ed95f387e3d4be1fe453e4acb7d0.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1205694195" 
	@${RM} ${OBJECTDIR}/_ext/1205694195/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1205694195/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1205694195/interrupts.o.d" -o ${OBJECTDIR}/_ext/1205694195/interrupts.o ../src/config/pic32mz_ef_curiosity2/interrupts.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1205694195/exceptions.o: ../src/config/pic32mz_ef_curiosity2/exceptions.c  .generated_files/4501e597d9c8f69087b014e10ff2f47a63ad128f.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1205694195" 
	@${RM} ${OBJECTDIR}/_ext/1205694195/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1205694195/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1205694195/exceptions.o.d" -o ${OBJECTDIR}/_ext/1205694195/exceptions.o ../src/config/pic32mz_ef_curiosity2/exceptions.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/20ffaa638e3bdefbc750f493a048e3f08bc1d065.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
else
${OBJECTDIR}/_ext/1746154667/plib_clk.o: ../src/config/pic32mz_ef_curiosity2/peripheral/clk/plib_clk.c  .generated_files/55387aaedf5e897e5c842e04d053328e73424115.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1746154667" 
	@${RM} ${OBJECTDIR}/_ext/1746154667/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1746154667/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1746154667/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1746154667/plib_clk.o ../src/config/pic32mz_ef_curiosity2/peripheral/clk/plib_clk.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1703710942/plib_evic.o: ../src/config/pic32mz_ef_curiosity2/peripheral/evic/plib_evic.c  .generated_files/2c563f55d982e1928b8568641d0bdd2b25e43728.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1703710942" 
	@${RM} ${OBJECTDIR}/_ext/1703710942/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1703710942/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1703710942/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1703710942/plib_evic.o ../src/config/pic32mz_ef_curiosity2/peripheral/evic/plib_evic.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1703657114/plib_gpio.o: ../src/config/pic32mz_ef_curiosity2/peripheral/gpio/plib_gpio.c  .generated_files/5ec0dd8d8b1fca1a3e5ecb5282cc91a82af1e073.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1703657114" 
	@${RM} ${OBJECTDIR}/_ext/1703657114/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1703657114/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1703657114/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1703657114/plib_gpio.o ../src/config/pic32mz_ef_curiosity2/peripheral/gpio/plib_gpio.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1703254171/plib_uart6.o: ../src/config/pic32mz_ef_curiosity2/peripheral/uart/plib_uart6.c  .generated_files/49a09bde926a13904971ca01176d17da57dc294f.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1703254171" 
	@${RM} ${OBJECTDIR}/_ext/1703254171/plib_uart6.o.d 
	@${RM} ${OBJECTDIR}/_ext/1703254171/plib_uart6.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1703254171/plib_uart6.o.d" -o ${OBJECTDIR}/_ext/1703254171/plib_uart6.o ../src/config/pic32mz_ef_curiosity2/peripheral/uart/plib_uart6.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1461993363/xc32_monitor.o: ../src/config/pic32mz_ef_curiosity2/stdio/xc32_monitor.c  .generated_files/3fdc6ba57f309b4f45619746d97990c3239f3a49.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1461993363" 
	@${RM} ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1461993363/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/1461993363/xc32_monitor.o ../src/config/pic32mz_ef_curiosity2/stdio/xc32_monitor.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1205694195/initialization.o: ../src/config/pic32mz_ef_curiosity2/initialization.c  .generated_files/79be85a37e3c316a754179e08e99d474e67aaac0.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1205694195" 
	@${RM} ${OBJECTDIR}/_ext/1205694195/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1205694195/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1205694195/initialization.o.d" -o ${OBJECTDIR}/_ext/1205694195/initialization.o ../src/config/pic32mz_ef_curiosity2/initialization.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1205694195/interrupts.o: ../src/config/pic32mz_ef_curiosity2/interrupts.c  .generated_files/976d9915ee3b95bd00d7c4e8f37e755485c294d6.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1205694195" 
	@${RM} ${OBJECTDIR}/_ext/1205694195/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1205694195/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1205694195/interrupts.o.d" -o ${OBJECTDIR}/_ext/1205694195/interrupts.o ../src/config/pic32mz_ef_curiosity2/interrupts.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1205694195/exceptions.o: ../src/config/pic32mz_ef_curiosity2/exceptions.c  .generated_files/ec7127846ebc5bcfcd15e6243c99fd289776f94c.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1205694195" 
	@${RM} ${OBJECTDIR}/_ext/1205694195/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1205694195/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1205694195/exceptions.o.d" -o ${OBJECTDIR}/_ext/1205694195/exceptions.o ../src/config/pic32mz_ef_curiosity2/exceptions.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/dba72fc26949260bc9004e5702f5e71641a409b8.flag .generated_files/f23c2f7ada75baa5ce7496a92827b4ab71c91d4b.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/config/pic32mz_ef_curiosity2" -I"../src/packs/PIC32MZ2048EFM144_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/config/pic32mz_ef_curiosity2/p32MZ2048EFM144.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x37F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/config/pic32mz_ef_curiosity2/p32MZ2048EFM144.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_pic32mz_ef_curiosity2=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Hello_World_ForReal.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/pic32mz_ef_curiosity2
	${RM} -r dist/pic32mz_ef_curiosity2

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
